<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallevelogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('callevelogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
                        $table->string('callType');
                        $table->datetime('time_called');
                        $table->datetime('cached_until');
		});
                
                Schema::table('shipjumpstats', function(Blueprint $table){
                   
                        $table->foreign('callevelogs_id')
                                ->references('id')->on('callevelogs')
                                ->onDelete('cascade');
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('callevelogs');
	}

}
