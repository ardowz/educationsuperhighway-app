<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipjumpstatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shipjumpstats', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
                        $table->integer('solar_sys_id');
                        $table->integer('ships_jumped');
                        $table->integer('callevelogs_id')->unsigned();
                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shipjumpstats');
	}

}
