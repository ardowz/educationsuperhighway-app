<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class callEveLog extends Model {

	protected $table = 'callevelogs';
        
        public function shipJumpStat(){
            return $this->hasMany('App\shipJumpStat');
        }
        
        public static function getSelectRanges($apiPath){
            $callEveLogModel = self::getAllRange($apiPath);
            
            $outputArray = array();
            foreach($callEveLogModel as $entry){
                $outputArray[$entry['id']] = $entry['time_called'].' - '.$entry['cached_until'];
            }
            
            return $outputArray;
        }
        
        public static function isPastCache($apiPath,$TZ = FALSE){
            //TZ will be for timezone?
            
            $currentCall = self::Api($apiPath)->GreaterThanCache()->first();
            if(is_null($currentCall)){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        public function scopeApi($query,$apiPath){
            return $query->where('callType','=',$apiPath);
        }
        
        public function scopeGreaterThanCache($query){
            //since time is UTC + 30 minutes
            $currentTime = date('Y-m-d H:m:s',strtotime("+30 minutes"));
//            var_dump($currentTime);
            return $query->where('cached_until','>',$currentTime);
        }

        public static function getAllRange($apiPath){
            $currentCall = self::Api($apiPath)->get();
            
            $outputArray = array();
            foreach($currentCall as $logEntry){
                $outputArray[] = array(
                    'id' => $logEntry['id'],
                    'time_called' => $logEntry['time_called'],
                    'cached_until' => $logEntry['cached_until'],
                );
            }
            
            return $outputArray;
        }
}
