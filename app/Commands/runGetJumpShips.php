<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use GuzzleHttp\Client;

class runGetJumpShips extends Command implements SelfHandling {

    
        protected $serverAddress = "http://ehs.app/api/getMapJumps";
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		//
            $client = new Client();
            $response = $client->get($this->serverAddress);
	}

}
