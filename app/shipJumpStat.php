<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class shipJumpStat extends Model {

	//
        protected $table = 'shipjumpstats';
        
        public function solarName(){
            return $this->belongsTo('App\solarName');
        }
        
        public static function getAllLogs($apiPath){
            $callEveLogsArray = \App\callEveLog::getAllRange($apiPath);
            
            foreach($callEveLogsArray as &$callEveLogRange){
                $callEveLogRange['logs'] = self::getStatInArray($callEveLogRange['id']);
            }
            
            return $callEveLogsArray;
        }
        
        public static function getAllAvgPerHour($apiPath,$orderBy = 'desc'){
            
            /*
             * Unsure within the 2
             * ->join('solarnames','shipjumpstats.solar_sys_id','=','solarnames.id')
             */
            $results = \Illuminate\Support\Facades\DB::select("select avg(average) as avgPerAllHr,solar_sys_id, sn.name 
from
(
select hourEntry, avg(jumps) as average, solar_sys_id
from
(
select count(*) as jumps, sjs.solar_sys_id,
 date(cel.time_called) as dayEntry,hour(cel.time_called) as hourEntry
from shipjumpstats as sjs
join callevelogs as cel
on (sjs.callevelogs_id = cel.id)
group by sjs.solar_sys_id,dayEntry,hourEntry
) countPerHr
group by hourEntry, solar_sys_id
) avgPerHr
join solarnames as sn
on (sn.id = avgPerHr.solar_sys_id)
group by solar_sys_id
order by avgPerAllHr $orderBy 
limit 10");
            
            /*
            $results = \Illuminate\Support\Facades\DB::select(
                    "select avg(average) as avgPerAllHr,solar_sys_id, sum(total) as total
from
(
select hourEntry, avg(jumps) as average, solar_sys_id, sum(jumps) as total
from
(
select count(*) as jumps, sjs.solar_sys_id,
 date(cel.time_called) as dayEntry,hour(cel.time_called) as hourEntry
from shipjumpstats as sjs
join callevelogs as cel
on (sjs.callevelogs_id = cel.id)
group by sjs.solar_sys_id,dayEntry,hourEntry
) countPerHr
group by solar_sys_id, hourEntry
) avgPerHr
group by solar_sys_id
order by total desc,avgPerAllHr $orderBy
limit 10"
                    );
             * 
             */
            
            $outputArray = array();
            foreach($results as $row){
                $outputArray[] = array(
                  'avgPerHr' => $row->avgPerAllHr,
                   'solar_sys_id' => $row->solar_sys_id,
                   'name' => $row->name
                );
            }
            
            return $outputArray;
        }
        
        public static function getDataPointsCountAllAvg($apiPath,$orderBy = 'desc'){
            $results = self::getAllAvgPerHour($apiPath,$orderBy);
            $solarSysIDs = array();
            foreach($results as $row){
                $solarSysIDs[] = $row['solar_sys_id'];
            }
            
            $solarSysIDString = implode(',',$solarSysIDs);
            $whereInSolarSysID = "(".$solarSysIDString.")";
            $results = \Illuminate\Support\Facades\DB::select(
                    "select count(*) as datapointCount,sjs.solar_sys_id, sn.name  from shipjumpstats as sjs"
                    . " join solarnames as sn on (sn.id = sjs.solar_sys_id) "
                    . " where sjs.solar_sys_id in $whereInSolarSysID "
                    . " group by sjs.solar_sys_id order by FIELD(sjs.solar_sys_id,$solarSysIDString)"
                    );
            
            $outputArray = array();
            foreach($results as $row){
                $outputArray[] = array(
                    'dataPointCount' => $row->datapointCount,
//                    'solar_sys_id' => $row->solar_sys_id,
                    'solar_sys_id' => $row->name,
                );
            }
            
            return $outputArray;
        }
        
        public static function getMostAndLeastActive($apiPath){
            $callEveLogsArray = \App\callEveLog::getAllRange($apiPath);
            
            foreach($callEveLogsArray as &$callEveLogRange){
                $callEveLogRange['most_active'] = self::getMostActiveInArray($callEveLogRange['id']);
                $callEveLogRange['least_active'] = self::getLeastActiveInArray($callEveLogRange['id']);
            }
            
            return $callEveLogRange;
        }
        
        private static function entryArray($result){
            return array(
//               'solar_sys_id' => $result['solar_sys_id'],
               'solar_sys_id' => $result['name'],
               'ships_jumped' => $result['ships_jumped'],  
            );
        }
        
        public static function getMostActiveInArray($callevelogID){
            $mostActiveModel = shipJumpStat::EveLog($callevelogID)->MostActive()->join('solarnames','shipjumpstats.solar_sys_id','=','solarnames.id')->take(10)->get();
            $outputArray = array();
            foreach($mostActiveModel as $activeEntry){
                $outputArray[] = self::entryArray($activeEntry);
            }
            
            return $outputArray;
        }
        
        public static function getLeastActiveInArray($callevelogID){
            $leastActiveModel = shipJumpStat::EveLog($callevelogID)->MoreThanOneJump()->LeastActive()->join('solarnames','shipjumpstats.solar_sys_id','=','solarnames.id')->take(10)->get();
            $outputArray = array();
            foreach($leastActiveModel as $activeEntry){
                $outputArray[] = self::entryArray($activeEntry);
            }
            
            return $outputArray;
        }
        
        public function scopeMostActive($query){
            return $query->orderBy('ships_jumped','desc');
        }
        
        public function scopeLeastActive($query){
            return $query->orderBy('ships_jumped','asc');
        }
        
        public function scopeMoreThanOneJump($query){
            return $query->where('ships_jumped','>',0);
        }

        private static function getStatInArray($callevelogID){
            $outputArray = array();
            //possible lazy loading here
            $shipJumpStatModel = shipJumpStat::EveLog($callevelogID)->join('solarnames','shipjumpstats.solar_sys_id','=','solarnames.id')->take(30)->get();
//            $shipJumpStatModel = shipJumpStat::EveLog($callevelogID)->get();
            foreach($shipJumpStatModel as $shipJumpEntry){
                $outputArray[] = self::entryArray($shipJumpEntry);
            }
            return $outputArray;
        }
        
        public function scopeEveLog($query,$callevelog_id){
            return $query->where('callevelogs_id','=',$callevelog_id);
        }
}
