<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('api/getMapJumps', array('as' => 'getMapJumps', 'uses' => 'EveController@callMapJumps'));
Route::get('part1', array('as' => 'part1','uses'=>'ChallengeController@viewPart1'));
Route::get('part2', array('as' => 'part2','uses'=>'ChallengeController@viewPart2'));
Route::get('part3', array('as' => 'part3','uses'=>'ChallengeController@viewPart3'));

Route::get('getMostActiveStationData/{callevelogs_id?}', array('as' => 'mostActiveStation', 'uses' => 'EveController@getMostActiveStation'));
Route::get('getLeastActiveStationData/{callevelogs_id?}', array('as' => 'leastActiveStation', 'uses' => 'EveController@getLeastActiveStation'));

Route::get('getConsistentActiveStation', array('as'=>'consistentActiveStation','uses'=>'EveController@getConsistentActiveStation'));
Route::get('getConsistentInactiveStation', array('as'=>'consistentInactiveStation','uses'=>'EveController@getConsistentInactiveStation'));