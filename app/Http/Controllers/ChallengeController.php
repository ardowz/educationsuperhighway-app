<?php

namespace App\Http\Controllers;


class ChallengeController extends Controller {
    
    
    public function viewPart1(){
        $apiPath = '/map/Jumps.xml.aspx';
        $allEveLog = \App\shipJumpStat::getAllLogs($apiPath);
        
        $view = view('challenge.part1',array(
            'data' => $allEveLog,
        ));
        return $view;
    }
    
    public function viewPart2(){
        $apiPath = '/map/Jumps.xml.aspx';
        
        $allActiveLog = \App\shipJumpStat::getMostAndLeastActive($apiPath);
        
        $selection = \App\callEveLog::getSelectRanges($apiPath);
        
        $view = view('challenge.part2',array(
            'data' => $allActiveLog,
            'selection' => $selection,
        ));
        return $view;
    }
    
    public function viewPart3(){
        $apiPath = '/map/Jumps.xml.aspx';
        
        $consistentActiveDataPoints = \App\shipJumpStat::getDataPointsCountAllAvg($apiPath);
        $consistentInactiveDataPoints = \App\shipJumpStat::getDataPointsCountAllAvg($apiPath,'asc');
        
        $view = view('challenge.part3',array(
           'activeDp' => $consistentActiveDataPoints,
           'inactiveDp' => $consistentInactiveDataPoints,
        ));
        return $view;
    }
    
    public function viewPart4(){
        $apiPath = '/map/Jumps.xml.aspx';
        
        
    }
}