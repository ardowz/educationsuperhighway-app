<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class EveController extends Controller {
    
    protected $eveApiURI = 'https://api.eveonline.com';
    
    public function callMapJumps(){
        $apiPath = '/map/Jumps.xml.aspx';
        $mapJumpURI = $this->eveApiURI.$apiPath;
        
        //call eve api
        $client = new Client();
        $response = $client->get($mapJumpURI);
        
        if($response->getStatusCode()){
            $body = $response->xml();
            
            //times are in gmt
            $currentTime = $body->currentTime;
            $cachedUntil = $body->cachedUntil;
            
            $eveApiLogModel = \App\callEveLog::where('callType','=',$apiPath)->orderBy('id','desc')->first();
            if(is_null($eveApiLogModel)){
                //Create first entry
                $apiLogID = $this->createNewApiLog($apiPath,$currentTime,$cachedUntil);
            }else{
                $pastCache = \App\callEveLog::isPastCache($apiPath);
                
                if($pastCache){
                    $apiLogID = $this->createNewApiLog($apiPath,$currentTime,$cachedUntil);
                }else{
                    //dont do anything
                    echo "skipping same cached results";
                    exit();
                }
            }
            
            $results = $body->result;
            $ctr = 0;
            echo $currentTime;
            foreach($results->children() as $resultOut){
                foreach($resultOut->children() as $row){
                    $solarSystemID = (int)$row['solarSystemID'];
                    $shipJumps = (int)$row['shipJumps'];
                    $this->createNewShipJumpStat($apiLogID, $solarSystemID, $shipJumps);
                }
            }
            
        }else{
            echo "Error in posting";
            exit();
        }
        
        echo "api run complete";
        exit();
    }
    
    private function createNewApiLog($type,$timeCalled,$cachedUntil){
        $newEveApiLogModel = new \App\callEveLog();
        $newEveApiLogModel->callType = $type;
        $newEveApiLogModel->time_called = $timeCalled;
        $newEveApiLogModel->cached_until = $cachedUntil;
        $newEveApiLogModel->save();
        return $newEveApiLogModel->id;
    }
    
    private function createNewShipJumpStat($apiLogID,$solarSystemID,$shipJumps){
        $newShipJumpStats = new \App\shipJumpStat();
        $newShipJumpStats->solar_sys_id = $solarSystemID;
        $newShipJumpStats->ships_jumped = $shipJumps;
        $newShipJumpStats->callevelogs_id = $apiLogID;
        $newShipJumpStats->save();
        return TRUE;
    }
    
    public function getActiveData(){
        echo "date	close
1-May-12	582.13
30-Apr-12	583.98
27-Apr-12	603.00
26-Apr-12	607.70
25-Apr-12	610.00
24-Apr-12	560.28
23-Apr-12	571.70
20-Apr-12	572.98
19-Apr-12	587.44
18-Apr-12	608.34
17-Apr-12	609.70";
    }
    
    public function getMostActiveStation($callevelogs_id = false){
        $apiPath = '/map/Jumps.xml.aspx';
        
//        $allActiveLog = \App\shipJumpStat::getMostAndLeastActive($apiPath);
        $mostActive = \App\shipJumpStat::getMostActiveInArray($callevelogs_id);

        $this->outputToJson($mostActive,'Most Active Set');
    }
    
    public function getLeastActiveStation($callevelogs_id = false){
        $apiPath = '/map/Jumps.xml.aspx';
        
//        $allActiveLog = \App\shipJumpStat::getMostAndLeastActive($apiPath);
        $leastActive = \App\shipJumpStat::getLeastActiveInArray($callevelogs_id);
        
        $this->outputToJson($leastActive,'Least Active Set');
    }
    
    public function getConsistentActiveStation(){
        $apiPath = '/map/Jumps.xml.aspx';
        
        $consistentActive = \App\shipJumpStat::getAllAvgPerHour($apiPath);
     
        $this->outputToJson($this->modifyKeyToWorkWithOutputJson($consistentActive), 'Consistently Active Stations');
    }
    
    public function getConsistentInactiveStation(){
        $apiPath = '/map/Jumps.xml.aspx';
        
        $consistentActive = \App\shipJumpStat::getAllAvgPerHour($apiPath,'asc');
     
        $this->outputToJson($this->modifyKeyToWorkWithOutputJson($consistentActive), 'Consistently Inactive Stations');
    }
    
    private function modifyKeyToWorkWithOutputJson($array){
        //to be used by consistency output
        $outputArray = array();
        foreach($array as $entry){
            $outputArray[] = array(
//                'solar_sys_id' => $entry['solar_sys_id'], //this will be used for x axis
                'solar_sys_id' => $entry['name'], //this will be used for x axis
                'ships_jumped' => $entry['avgPerHr'], // this is for y axis
            );
        }
        
        return $outputArray;
    }
    
    public function outputToJson($array,$label){
        $labelsArray = array();
        $dataArray = array();
        foreach($array as $entry){
            $labelsArray[] = $entry['solar_sys_id'];
            $dataArray[] = $entry['ships_jumped'];
        }
        
        $dataSetsObject = new \stdClass();
        $dataSetsObject->label = $label;
        $dataSetsObject->fillColor = "rgba(220,220,220,0.2)";
        $dataSetsObject->strokeColor = "rgba(220,220,220,1)";
        $dataSetsObject->pointColor = "rgba(220,220,220,1)";
        $dataSetsObject->pointStrokeColor = "#fff";
        $dataSetsObject->pointHighlightFill = "#fff";
        $dataSetsObject->pointHighlightStroke = "rgba(220,220,220,1)";
        $dataSetsObject->data = $dataArray;
        
        $outputArray = array(
            'labels' => $labelsArray,
            'datasets' => array(
                $dataSetsObject
            ),
        );
        
        echo json_encode($outputArray);
    }
    
    private function outputToTSV($array){
       $header = array('SolarSystemID','ShipsJumped');
       $newBodyArray = array();
       foreach($array as $entry){
           $newBodyArray[] = array(
               $entry['solar_sys_id'], $entry['ships_jumped'],
           );
       }
       
       $tabbedHeader = implode("\t",$header);
       $tabbedBodyArray = array($tabbedHeader);
       foreach($newBodyArray as $entry){
           $tabbedBodyArray[] = implode("\t", $entry);
       }
       
       $tsvOut = implode("\r\n", $tabbedBodyArray);
       
//       header('Content-Encoding: UTF-8');
//       header('Content-type: text/tsv; charset=UTF-8');
       echo $tsvOut;
//       var_dump($header,$tsvOut);
    }
}