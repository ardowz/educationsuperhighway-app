## EducationSuperHighway Coding Challenge

This webapp is written on PHP with Laravel Framework.

To deploy the app, I used one of Laravel's easiest deployment which is using Homestead. Which can be found at http://laravel.com/docs/5.0/homestead .
Once Homestead is installed, some configuration has to be set. 

# Set the "sites:" portion to
    sites:
        - map: esh.app
          to: /home/vagrant/Code/esh/public

# Modify host computer's hosts file to point to homestead using esh.app
# Modify /etc/hosts and add 127.0.0.1 esh.app
# run composer update in the source code folder
# run composer require guzzlehttp/guzzle
# while in the source code folder, run: php artisan migrate --seed
# Create crontab -e with : * * * * * php /home/vagrant/Code/esh/artisan schedule:run 1>> /dev/null 2>&1

## The routes are set up by parts
# http://esh.app/part1
# http://esh.app/part2
# http://esh.app/part3
# http://esh.app/part4 (did not finish)


#Warning: Please open readme.md when doing any form of copy & pasting since some character don't show up in bitbucket.