<html>
    <head>
        @include('scripts.scripts')
    </head>
    <body>
        <table>
            <tr>
                <td>Range Selector</td>
                <td><select id="chartSelection">
                    @foreach($selection as $id => $value)
                    <option value="{{ $id }}">{{ $value }}</option>
                    @endforeach
                    </select></td>
            </tr>
        </table>
        <table>
            <thead>
                <tr>
                    <td>Most Active Systems</td>
                    <td>Least Active Systems</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><canvas id="myChart" width="600" height="600"></canvas></td>
                    <td><canvas id="leadActiveChart" width="600" height="600"></canvas></td>
                </tr>
            </tbody>
        </table>
        
        
        <script>
            // Get context with jQuery - using jQuery's .get() method.
            var ctx = $("#myChart").get(0).getContext("2d");
            var leastActive = $("#leadActiveChart").get(0).getContext("2d");
            
            var data2 = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        }
    ]
};
        $( document ).ready(function() {
            
            var currentSelectedID = $('#chartSelection').attr('value');
            
            var mostActiveURL = "getMostActiveStationData/";
            var leastActiveURL = "getLeastActiveStationData/";
            
            var callMostActiveURL = mostActiveURL+currentSelectedID;
            var callLeastActiveURL = leastActiveURL+currentSelectedID;
            
            $.get(callMostActiveURL).success(function(data){
               var options = {
                    scaleBeginAtZero : false,
                    pointDot: true,
                    datasetStroke: true,
                    bezierCurve: false
               };
               
               data = JSON.parse(data);
               // This will get the first returned node in the jQuery collection.
               var myLineChart = new Chart(ctx).Line(data,options);
            });
            
            $.get(callLeastActiveURL).success(function(data){
               var options = {
                    scaleBeginAtZero : false,
                    pointDot: true,
                    datasetStroke: true,
                    bezierCurve: false
               };
               
               data = JSON.parse(data);
               var leastActiveChart = new Chart(leastActive).Line(data,options);
           });
           
           
           $("#chartSelection").change(function() {
                var id = $(this).children(":selected").attr("value");
                
                callMostActiveURL = mostActiveURL+id;
                callLeastActiveURL = leastActiveURL+id;
                
                $.get(callMostActiveURL).success(function(data){
                    var options = {
                         scaleBeginAtZero : false,
                         pointDot: true,
                         datasetStroke: true,
                         bezierCurve: false
                    };

                    data = JSON.parse(data);
                    // This will get the first returned node in the jQuery collection.
                    var myLineChart = new Chart(ctx).Line(data,options);
                 });

                 $.get(callLeastActiveURL).success(function(data){
                    var options = {
                         scaleBeginAtZero : false,
                         pointDot: true,
                         datasetStroke: true,
                         bezierCurve: false
                    };

                    data = JSON.parse(data);
                    var leastActiveChart = new Chart(leastActive).Line(data,options);
                });
                
            });
        });
    </script>
    </body>
</html>

