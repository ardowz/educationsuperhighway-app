<html>
    <head>
        @include('scripts.scripts')
    </head>
    <body>
        <h2>This shows what the average jumps per station per hour. The highest average dictates having the most consistent activity</h2>
        <table>
            <thead>
                <tr>
                    <td>Consistently Active Systems</td>
                    <td>Consistently Inactive Systems</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><canvas id="myChart" width="600" height="600"></canvas></td>
                    <td><canvas id="leadActiveChart" width="600" height="600"></canvas></td>
                </tr>
                <tr>
                    <td>
                        <h3>Datapoint Count: </h3>
                        <ul>
                            @foreach($activeDp as $entry)
                            <li>{{ $entry['solar_sys_id'] }} : {{ $entry['dataPointCount'] }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <h3>Datapoint Count: </h3>
                        <ul>
                            @foreach($inactiveDp as $entry)
                            <li>{{ $entry['solar_sys_id'] }} : {{ $entry['dataPointCount'] }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                
            </tbody>
        </table>
        
        
        <script>
            // Get context with jQuery - using jQuery's .get() method.
            var ctx = $("#myChart").get(0).getContext("2d");
            var leastActive = $("#leadActiveChart").get(0).getContext("2d");
            
            var data2 = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        }
    ]
};
        $( document ).ready(function() {
            
            var currentSelectedID = $('#chartSelection').attr('value');
            
            var consistentlyActiveURL = "getConsistentActiveStation";
            var consistentlyInactiveURL = "getConsistentInactiveStation";
            
            
            $.get(consistentlyActiveURL).success(function(data){
               var options = {
                    scaleBeginAtZero : false,
                    pointDot: true,
                    datasetStroke: true,
                    bezierCurve: false
               };
               
               data = JSON.parse(data);
               // This will get the first returned node in the jQuery collection.
               var myLineChart = new Chart(ctx).Line(data,options);
            });
            
            $.get(consistentlyInactiveURL).success(function(data){
               var options = {
                    scaleBeginAtZero : false,
                    pointDot: true,
                    datasetStroke: true,
                    bezierCurve: false
               };
               
               data = JSON.parse(data);
               var leastActiveChart = new Chart(leastActive).Line(data,options);
           });
           
           
        });
    </script>
    </body>
</html>

