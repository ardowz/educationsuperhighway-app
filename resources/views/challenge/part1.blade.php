<html>
    <body>
        
        @foreach($data as $range)
        <div>
        <table>
            <tbody>
                <tr>
                    <td>Period Started</td>
                    <td>{{ $range['time_called'] }}</td>
                </tr>
                <tr>
                    <td>Period Ended</td>
                    <td>{{ $range['cached_until'] }}</td>
                </tr>
            </tbody>
        </table>
        
            
            <div>    
                <table border='1'>
                    <thead>
                        <tr>
                            <td>Solar System</td>
                            <td>Ships Jumped</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($range['logs'] as $logs)
                        <tr>
                            <td>{{ $logs['solar_sys_id'] }}</td>
                            <td>{{ $logs['ships_jumped'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        
        <br/>
        </div>
        @endforeach
        
    </body>
</html>